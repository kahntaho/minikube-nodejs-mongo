Simple project following this guide: https://developer.ibm.com/recipes/tutorials/kubernetes-how-to-run-a-node-js-application-which-accesses-mongo-database-where-both-are-running-in-containers-in-different-pods/

Steps:
1. kubectl apply -f pod-mongo.yml
2. kubectl apply -f pod-employee.yml
3. minikube ip
4. http://{minikube ip}:{service nodejs nodeport}
